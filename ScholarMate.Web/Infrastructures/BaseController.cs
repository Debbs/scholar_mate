﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ScholarMate.Model.Models;
using ScholarMate.Model.Models.Users;
using ScholarMate.Web.Services;

namespace ScholarMate.Infrastructures
{
    public class BaseController : Controller
    {

        protected readonly ScholarMateContext _ctx;

        protected UserManager<ApplicationUser> _userManager;
        protected SignInManager<ApplicationUser> _signInManager;
        protected IEmailSender _emailSender;
        protected RoleManager<ApplicationRole> _roleManager;


        public BaseController(ScholarMateContext ctx)
        {
            _ctx = ctx;
        }

        public BaseController(
            ScholarMateContext ctx,
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            RoleManager<ApplicationRole> roleManager,
            IEmailSender emailSender)
        {
            _ctx = ctx;
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _emailSender = emailSender;
        }

    }
}