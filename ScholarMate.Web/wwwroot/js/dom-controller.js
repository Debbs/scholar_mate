﻿
//contains functions that manipulate elements in the DOM eg hiding, removing, showing etc

//remove an item row
function remove(obj) {
    var row = document.getElementById(obj.dataset.rowid);
    row.parentNode.removeChild(row);
}

$(function () { // .ready() callback, is only executed when the DOM is fully loaded
    var modalBtn = document.getElementById("showModal");
    modalBtn.addEventListener("click", function() {
        //get those in list
        var items = document.getElementsByClassName("feeitems");
        var i;
        var arr = [];
        for (i = 0; i < items.length; i++) {
            arr.push(parseInt(items[i].dataset.feeitemid));
        }
        var schoolid = modalBtn.dataset.schoolid;
        var url = modalBtn.dataset.url;
        $.ajax({
            type: "GET",
            url: url,
            //contentType: "application/json; charset=utf-8",
            data: { schoolid: schoolid, selectedItems: arr},
            //dataType: "json",
            traditional: true,
            success: function (data) {
                if (data.length > 0) {
                    var modalbody = document.getElementById("modalbody");
                    modalbody.innerText = "";
                    var ul = document.createElement("ul");
                    ul.className = "list-group";
                    for (i = 0; i < data.length; i++) {
                        var li = document.createElement("li");
                        li.className = "list-group-item justify-content-between align-items-center";
                        var textnode = document.createTextNode(data[i].name);
                        li.appendChild(textnode);
                        var checkbox = document.createElement("input");
                        checkbox.setAttribute("type", "checkbox");
                        checkbox.setAttribute("data-id", data[i].id);
                        checkbox.setAttribute("data-name", data[i].name);
                        checkbox.setAttribute("onchange", "addFeeItem(this)");
                        checkbox.className = "text-right";
                        li.appendChild(checkbox);
                        ul.appendChild(li);
                    }
                    modalbody.appendChild(ul);
                    $('#listModal').modal('show');
                }
            }
        });
    });
});


function addFeeItem(obj) {
    alert(obj.dataset.name);
}

$('#listModal').on('hide.bs.modal', function (e) {
    // do something...
})