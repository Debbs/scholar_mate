﻿
             //validate form entries before submission
$("form").submit(function (event) {
    //check if there are fee items before submitting
    var items = document.getElementsByClassName("feeitems");
    if (items.length > 0) {
        return;
    }
    $("#validation-msg").text("No items. You cannot save empty fees.").show().fadeOut(3000);
    event.preventDefault();
});