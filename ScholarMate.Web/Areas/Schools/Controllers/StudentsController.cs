﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ScholarMate.Infrastructures;
using ScholarMate.Model.Models;
using ScholarMate.Model.Models.Schools;

namespace ScholarMate.Web.Areas.Schools.Controllers
{
    [Authorize]
    [Area("Schools")]
    public class StudentsController : BaseController
    {

        public StudentsController(ScholarMateContext ctx) 
            : base(ctx)
        {
        }

        // GET: Schools/Students
        public async Task<IActionResult> Index()
        {
            var scholarMateContext = _ctx.Students.Include(s => s.Class).Include(s => s.School);
            return View(await scholarMateContext.ToListAsync());
        }

        // GET: Schools/Students/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var student = await _ctx.Students
                .Include(s => s.Class)
                .Include(s => s.School)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (student == null)
            {
                return NotFound();
            }

            return View(student);
        }

        // GET: Schools/Students/Create
        public IActionResult Create()
        {
            ViewData["ClassId"] = new SelectList(_ctx.Classes, "Id", "Name");
            return View();
        }

        // POST: Schools/Students/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("SchoolId,UniqueId,DateOfBirth,DateOfRegistration,DateOfCompletion,Name,ClassId")] Student student)
        {
            if (ModelState.IsValid)
            {
                student.CreatedOn = DateTime.Now;
                student.UpdatedOn = DateTime.Now;
                _ctx.Add(student);
                await _ctx.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClassId"] = new SelectList(_ctx.Classes, "Id", "Name", student.ClassId);
            return View(student);
        }

        // GET: Schools/Students/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var student = await _ctx.Students.SingleOrDefaultAsync(m => m.Id == id);
            if (student == null)
            {
                return NotFound();
            }
            ViewData["ClassId"] = new SelectList(_ctx.Classes, "Id", "Name", student.ClassId);
            return View(student);
        }

        // POST: Schools/Students/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,SchoolId,UniqueId,DateOfBirth,DateOfRegistration,DateOfCompletion,Name,ClassId")] Student student)
        {
            if (id != student.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    //get object from database
                    var eStudent = _ctx.Students.Find(id);
                    //update modelstate object with the appropriate values 
                    student.CreatedOn = eStudent.CreatedOn; //createdOn date for an edited object must not change. So maintain.
                    student.UpdatedOn = DateTime.Now;
                    //update database object with modelstate object
                    _ctx.Entry(eStudent).CurrentValues.SetValues(student);
                    _ctx.Entry(eStudent).State = EntityState.Modified;
                    _ctx.Update(eStudent);
                    await _ctx.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StudentExists(student.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClassId"] = new SelectList(_ctx.Classes, "Id", "Name", student.ClassId);
            return View(student);
        }

        // GET: Schools/Students/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var student = await _ctx.Students
                .Include(s => s.Class)
                .Include(s => s.School)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (student == null)
            {
                return NotFound();
            }

            return View(student);
        }

        // POST: Schools/Students/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var student = await _ctx.Students.SingleOrDefaultAsync(m => m.Id == id);
            _ctx.Students.Remove(student);
            await _ctx.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool StudentExists(int id)
        {
            return _ctx.Students.Any(e => e.Id == id);
        }
    }
}
