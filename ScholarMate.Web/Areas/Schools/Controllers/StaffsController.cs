﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ScholarMate.Infrastructures;
using ScholarMate.Model.Models;
using ScholarMate.Model.Models.Schools;

namespace ScholarMate.Web.Areas.Schools.Controllers
{
    [Area("Schools")]
    public class StaffsController : BaseController
    {

        public StaffsController(ScholarMateContext ctx) 
            : base(ctx)
        {
        }

        // GET: Schools/Staffs
        public async Task<IActionResult> Index()
        {
            var scholarMateContext = _ctx.Staffs.Include(s => s.School);
            return View(await scholarMateContext.ToListAsync());
        }

        // GET: Schools/Staffs/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var staff = await _ctx.Staffs
                .Include(s => s.School)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (staff == null)
            {
                return NotFound();
            }

            return View(staff);
        }

        // GET: Schools/Staffs/Create
        public IActionResult Create()
        {
            ViewData["SchoolId"] = new SelectList(_ctx.Schools, "Id", "Name");
            return View();
        }

        // POST: Schools/Staffs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("SchoolId,UniqueId,DateOfBirth,DateOfRecruitment,Name,IsTeaching")] Staff staff)
        {
            if (ModelState.IsValid)
            {
                staff.CreatedOn = DateTime.Now;
                staff.UpdatedOn = DateTime.Now;
                _ctx.Add(staff);
                await _ctx.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(staff);
        }

        // GET: Schools/Staffs/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var staff = await _ctx.Staffs.SingleOrDefaultAsync(m => m.Id == id);
            if (staff == null)
            {
                return NotFound();
            }
            return View(staff);
        }

        // POST: Schools/Staffs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,SchoolId,UniqueId,DateOfBirth,DateOfRecruitment,Name,IsTeaching")] Staff staff)
        {
            if (id != staff.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    //get object from database
                    var eStaff = _ctx.Staffs.Find(id);
                    //update modelstate object with the appropriate values 
                    staff.CreatedOn = eStaff.CreatedOn; //createdOn date for an edited object must not change. So maintain.
                    staff.UpdatedOn = DateTime.Now; 
                    //update database object with modelstate object
                    _ctx.Entry(eStaff).CurrentValues.SetValues(staff);
                    _ctx.Entry(eStaff).State = EntityState.Modified;
                    _ctx.Update(eStaff);
                    await _ctx.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StaffExists(staff.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(staff);
        }

        // GET: Schools/Staffs/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var staff = await _ctx.Staffs
                .Include(s => s.School)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (staff == null)
            {
                return NotFound();
            }

            return View(staff);
        }

        // POST: Schools/Staffs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var staff = await _ctx.Staffs.SingleOrDefaultAsync(m => m.Id == id);
            _ctx.Staffs.Remove(staff);
            await _ctx.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool StaffExists(int id)
        {
            return _ctx.Staffs.Any(e => e.Id == id);
        }
    }
}
