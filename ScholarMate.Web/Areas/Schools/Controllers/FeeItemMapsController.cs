﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ScholarMate.Model.Models;
using ScholarMate.Model.Models.Fees;

namespace ScholarMate.Web.Areas.Schools.Controllers
{
    [Area("Schools")]
    public class FeeItemMapsController : Controller
    {
        private readonly ScholarMateContext _context;

        public FeeItemMapsController(ScholarMateContext context)
        {
            _context = context;
        }

        // GET: Schools/FeeItemMaps
        public async Task<IActionResult> Index()
        {
            var scholarMateContext = _context.FeeItemMaps.Include(f => f.Fee).Include(f => f.FeeItem).Include(f => f.School);
            return View(await scholarMateContext.ToListAsync());
        }

        // GET: Schools/FeeItemMaps/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var feeItemMap = await _context.FeeItemMaps
                .Include(f => f.Fee)
                .Include(f => f.FeeItem)
                .Include(f => f.School)
                .SingleOrDefaultAsync(m => m.SchoolId == id);
            if (feeItemMap == null)
            {
                return NotFound();
            }

            return View(feeItemMap);
        }

        // GET: Schools/FeeItemMaps/Create
        public IActionResult Create()
        {
            ViewData["FeeId"] = new SelectList(_context.Fees, "Id", "Name");
            ViewData["FeeItemId"] = new SelectList(_context.FeeItems, "Id", "Name");
            return View();
        }

        // POST: Schools/FeeItemMaps/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("SchoolId,FeeId,FeeItemId,Amount,Timestamp,CreatedOn,UpdatedOn")] FeeItemMap feeItemMap)
        {
            if (ModelState.IsValid)
            {
                _context.Add(feeItemMap);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["FeeId"] = new SelectList(_context.Fees, "Id", "Name", feeItemMap.FeeId);
            ViewData["FeeItemId"] = new SelectList(_context.FeeItems, "Id", "Name", feeItemMap.FeeItemId);
            ViewData["SchoolId"] = new SelectList(_context.Schools, "Id", "Name", feeItemMap.SchoolId);
            return View(feeItemMap);
        }

        // GET: Schools/FeeItemMaps/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var feeItemMap = await _context.FeeItemMaps.SingleOrDefaultAsync(m => m.SchoolId == id);
            if (feeItemMap == null)
            {
                return NotFound();
            }
            ViewData["FeeId"] = new SelectList(_context.Fees, "Id", "Name", feeItemMap.FeeId);
            ViewData["FeeItemId"] = new SelectList(_context.FeeItems, "Id", "Name", feeItemMap.FeeItemId);
            ViewData["SchoolId"] = new SelectList(_context.Schools, "Id", "Name", feeItemMap.SchoolId);
            return View(feeItemMap);
        }

        // POST: Schools/FeeItemMaps/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("SchoolId,FeeId,FeeItemId,Amount,Timestamp,CreatedOn,UpdatedOn")] FeeItemMap feeItemMap)
        {
            if (id != feeItemMap.SchoolId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(feeItemMap);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FeeItemMapExists(feeItemMap.SchoolId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["FeeId"] = new SelectList(_context.Fees, "Id", "Name", feeItemMap.FeeId);
            ViewData["FeeItemId"] = new SelectList(_context.FeeItems, "Id", "Name", feeItemMap.FeeItemId);
            ViewData["SchoolId"] = new SelectList(_context.Schools, "Id", "Name", feeItemMap.SchoolId);
            return View(feeItemMap);
        }

        // GET: Schools/FeeItemMaps/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var feeItemMap = await _context.FeeItemMaps
                .Include(f => f.Fee)
                .Include(f => f.FeeItem)
                .Include(f => f.School)
                .SingleOrDefaultAsync(m => m.SchoolId == id);
            if (feeItemMap == null)
            {
                return NotFound();
            }

            return View(feeItemMap);
        }

        // POST: Schools/FeeItemMaps/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var feeItemMap = await _context.FeeItemMaps.SingleOrDefaultAsync(m => m.SchoolId == id);
            _context.FeeItemMaps.Remove(feeItemMap);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool FeeItemMapExists(int id)
        {
            return _context.FeeItemMaps.Any(e => e.SchoolId == id);
        }
    }
}
