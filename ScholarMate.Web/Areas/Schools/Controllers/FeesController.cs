﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ScholarMate.Infrastructures;
using ScholarMate.Model.Models;
using ScholarMate.Model.Models.Fees;

namespace ScholarMate.Web.Areas.Schools.Controllers
{
    [Area("Schools")]
    public class FeesController : BaseController
    {

        public FeesController(ScholarMateContext ctx) 
            : base(ctx)
        {
        }

        // GET: Schools/Fees
        public async Task<IActionResult> Index()
        {
            var scholarMateContext = _ctx.Fees.Include(f => f.AcademicYear).Include(f => f.School);
            return View(await scholarMateContext.ToListAsync());
        }

        // GET: Schools/Fees/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var fee = await _ctx.Fees
                .Include(f => f.AcademicYear)
                .Include(f => f.School)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (fee == null)
            {
                return NotFound();
            }

            return View(fee);
        }

        // GET: Schools/Fees/Create
        public IActionResult Create(bool? showInactive)
        {
            //for new fee, use all fee items in the itemize list.
            var feeItems = !showInactive.HasValue || !showInactive.Value ? _ctx.FeeItems.Where(x => x.Active) : _ctx.FeeItems;
            var fee = new Fee { FeeItems = new HashSet<FeeItemMap>() };
            fee.FeeItems = (from it in feeItems.OrderBy(o => o.SortIndex)
                              select new FeeItemMap
                              {
                                  FeeItemId = it.Id,
                                  FeeItem = it,
                                  SortIndex = it.SortIndex
                              }).ToHashSet();

            ViewData["AcademicYearId"] = new SelectList(_ctx.AcademicYears, "Id", "Name");
            return View(fee);
        }

        // POST: Schools/Fees/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("SchoolId,AcademicYearId,Name")] Fee fee, [Bind("FeeId, FeeItemId,Amount,SortIndex")] ICollection<FeeItemMap> FeeItemMaps)
        {
            if (ModelState.IsValid)
            {
                fee.FeeItems = new HashSet<FeeItemMap>();
                fee.CreatedOn = DateTime.Now;
                fee.UpdatedOn = DateTime.Now;
                foreach (var item in FeeItemMaps)
                {
                    item.CreatedOn = DateTime.Now;
                    item.UpdatedOn = DateTime.Now;
                    item.SchoolId = fee.SchoolId;
                    fee.FeeItems.Add(item);
                }
                _ctx.Add(fee);
                await _ctx.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["AcademicYearId"] = new SelectList(_ctx.AcademicYears, "Id", "Name", fee.AcademicYearId);
            return View(fee);
        }

        // GET: Schools/Fees/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var fee = await _ctx.Fees.Include(x => x.FeeItems)
                .ThenInclude(f => f.FeeItem)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (fee == null)
            {
                return NotFound();
            }
            ViewData["AcademicYearId"] = new SelectList(_ctx.AcademicYears, "Id", "Name", fee.AcademicYearId);
            return View(fee);
        }

        // POST: Schools/Fees/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,SchoolId,AcademicYearId,Name")] Fee fee, [Bind("Id,FeeId, FeeItemId,Amount,SortIndex")] ICollection<FeeItemMap> FeeItemMaps)
        {
            if (id != fee.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    //get object from database
                    var eFee = _ctx.Fees.Find(id);

                    //update modelstate object with the appropriate values 
                    eFee.Name = fee.Name;
                    eFee.AcademicYearId = fee.AcademicYearId;
                    eFee.UpdatedOn = DateTime.Now;
                    _ctx.Entry(eFee).State = EntityState.Modified;

                    var strategy = _ctx.Database.CreateExecutionStrategy();
                    await strategy.ExecuteAsync(async () =>
                    {
                        using (var ctrans = _ctx.Database.BeginTransaction())
                        {
                            try
                            {
                                //remove deleted items
                                var dels = _ctx.FeeItemMaps.Where(x => x.FeeId == fee.Id).Except(FeeItemMaps).ToList();
                                _ctx.FeeItemMaps.RemoveRange(dels);
                                foreach (var item in FeeItemMaps)
                                {
                                    var eFeeItem = _ctx.FeeItemMaps.Find(item.Id);
                                    eFeeItem.Amount = item.Amount;
                                    eFeeItem.UpdatedOn = DateTime.Now;
                                    _ctx.Entry(eFeeItem).State = EntityState.Modified;
                                }
                                await _ctx.SaveChangesAsync();
                                ctrans.Commit();
                            }catch(Exception ex) {
                                ctrans.Rollback();
                            }
                        }
                    });
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FeeExists(fee.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["AcademicYearId"] = new SelectList(_ctx.AcademicYears, "Id", "Name", fee.AcademicYearId);
            return View(fee);
        }

        // GET: Schools/Fees/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var fee = await _ctx.Fees
                .Include(f => f.AcademicYear)
                .Include(f => f.School)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (fee == null)
            {
                return NotFound();
            }

            return View(fee);
        }

        // POST: Schools/Fees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var fee = await _ctx.Fees.SingleOrDefaultAsync(m => m.Id == id);
            _ctx.Fees.Remove(fee);
            await _ctx.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool FeeExists(int id)
        {
            return _ctx.Fees.Any(e => e.Id == id);
        }

        // GET: Schools/Fees/FeeItems
        //[NonAction]
        public IActionResult FeeItems(int schoolId, int[] selectedItems)
        {
            var items = _ctx.FeeItems.Where(x => x.SchoolId == schoolId && !selectedItems.Any(y => y == x.Id)).OrderBy(o => o.SortIndex).ThenBy(o => o.Name).Select(x => new { x.Id, x.Name }).ToList();
            return new JsonResult(items);
        }
    }
}
