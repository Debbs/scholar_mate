﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ScholarMate.Infrastructures;
using ScholarMate.Model.Models;
using ScholarMate.Model.Models.Schools;

namespace ScholarMate.Web.Areas.Schools.Controllers
{
    [Area("Schools")]
    public class LevelsController : BaseController
    {

        public LevelsController(ScholarMateContext ctx) 
            : base(ctx)
        {
        }

        // GET: Schools/Levels
        public async Task<IActionResult> Index()
        {
            var scholarMateContext = _ctx.Levels.Include(l => l.School);
            return View(await scholarMateContext.ToListAsync());
        }

        // GET: Schools/Levels/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var level = await _ctx.Levels
                .Include(l => l.School)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (level == null)
            {
                return NotFound();
            }

            return View(level);
        }

        // GET: Schools/Levels/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Schools/Levels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("SchoolId,Name")] Level level)
        {
            if (ModelState.IsValid)
            {
                _ctx.Add(level);
                await _ctx.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(level);
        }

        // GET: Schools/Levels/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var level = await _ctx.Levels.SingleOrDefaultAsync(m => m.Id == id);
            if (level == null)
            {
                return NotFound();
            }
            return View(level);
        }

        // POST: Schools/Levels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,SchoolId,Name")] Level level)
        {
            if (id != level.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var eLevel = _ctx.Levels.Find(id);
                    _ctx.Entry(eLevel).CurrentValues.SetValues(level);
                    //eLevel.Timestamp = BitConverter.GetBytes(DateTime.Now.Ticks);
                    _ctx.Entry(eLevel).State = EntityState.Modified;
                    _ctx.Update(eLevel);
                    await _ctx.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LevelExists(level.Id))
                    {
                        return NotFound();
                    }
                }catch(Exception ex)
                {
                    throw ex;
                }
                return RedirectToAction(nameof(Index));
            }
            return View(level);
        }

        // GET: Schools/Levels/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var level = await _ctx.Levels
                .Include(l => l.School)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (level == null)
            {
                return NotFound();
            }

            return View(level);
        }

        // POST: Schools/Levels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var level = await _ctx.Levels.SingleOrDefaultAsync(m => m.Id == id);
            _ctx.Levels.Remove(level);
            await _ctx.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool LevelExists(int id)
        {
            return _ctx.Levels.Any(e => e.Id == id);
        }
    }
}
