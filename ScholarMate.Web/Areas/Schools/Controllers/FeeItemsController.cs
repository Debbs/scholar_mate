﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ScholarMate.Infrastructures;
using ScholarMate.Model.Models;
using ScholarMate.Model.Models.Fees;

namespace ScholarMate.Web.Areas.Schools.Controllers
{
    [Area("Schools")]
    public class FeeItemsController : BaseController
    {

        public FeeItemsController(ScholarMateContext ctx)
            : base(ctx)
        {
        }

        // GET: Schools/FeeItems
        public async Task<IActionResult> Index()
        {
            var scholarMateContext = _ctx.FeeItems.Include(f => f.School);
            return View(await scholarMateContext.ToListAsync());
        }

        // GET: Schools/FeeItems/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var feeItem = await _ctx.FeeItems
                .Include(f => f.School)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (feeItem == null)
            {
                return NotFound();
            }

            return View(feeItem);
        }

        // GET: Schools/FeeItems/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Schools/FeeItems/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("SchoolId,Name")] FeeItem feeItem)
        {
            if (ModelState.IsValid)
            {
                _ctx.Add(feeItem);
                await _ctx.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(feeItem);
        }

        // GET: Schools/FeeItems/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var feeItem = await _ctx.FeeItems.SingleOrDefaultAsync(m => m.Id == id);
            if (feeItem == null)
            {
                return NotFound();
            }
            return View(feeItem);
        }

        // POST: Schools/FeeItems/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("SchoolId,Name")] FeeItem feeItem)
        {
            if (id != feeItem.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {          
                    //get object from database
                    var eFeeItem = _ctx.FeeItems.Find(id);
                    //update database object with modelstate object
                    _ctx.Entry(eFeeItem).CurrentValues.SetValues(feeItem);
                    _ctx.Entry(eFeeItem).State = EntityState.Modified;
                    _ctx.Update(feeItem);
                    await _ctx.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FeeItemExists(feeItem.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(feeItem);
        }

        // GET: Schools/FeeItems/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var feeItem = await _ctx.FeeItems
                .Include(f => f.School)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (feeItem == null)
            {
                return NotFound();
            }

            return View(feeItem);
        }

        // POST: Schools/FeeItems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var feeItem = await _ctx.FeeItems.SingleOrDefaultAsync(m => m.Id == id);
            _ctx.FeeItems.Remove(feeItem);
            await _ctx.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool FeeItemExists(int id)
        {
            return _ctx.FeeItems.Any(e => e.Id == id);
        }
    }
}
