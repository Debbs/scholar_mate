﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ScholarMate.Infrastructures;
using ScholarMate.Model.Models;
using ScholarMate.Model.Models.Schools;

namespace ScholarMate.Web.Areas.Schools.Controllers
{
    [Area("Schools")]
    public class SchoolsController : BaseController
    {

        public SchoolsController(ScholarMateContext ctx) 
            : base(ctx)
        {
        }

        // GET: Schools/Schools
        public async Task<IActionResult> Index()
        {
            return View(await _ctx.Schools.ToListAsync());
        }

        // GET: Schools/Schools/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var school = await _ctx.Schools
                .SingleOrDefaultAsync(m => m.Id == id);
            if (school == null)
            {
                return NotFound();
            }

            return View(school);
        }

        // GET: Schools/Schools/Create
        public IActionResult Create()
        {
            ViewData["CurrentAcademicYearId"] = new SelectList(_ctx.AcademicYears, "Id", "Name");
            return View();
        }

        // POST: Schools/Schools/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,Logo,LogoHeight,LogoWidth,CurrentAcademicYearId")] School school)
        {
            if (ModelState.IsValid)
            {
                school.CreatedOn = DateTime.Now;
                school.UpdatedOn = DateTime.Now;
                _ctx.Add(school);
                await _ctx.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CurrentAcademicYearId"] = new SelectList(_ctx.AcademicYears, "Id", "Name");
            return View(school);
        }

        // GET: Schools/Schools/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var school = await _ctx.Schools.SingleOrDefaultAsync(m => m.Id == id);
            if (school == null)
            {
                return NotFound();
            }
            ViewData["CurrentAcademicYearId"] = new SelectList(_ctx.AcademicYears, "Id", "Name");
            return View(school);
        }

        // POST: Schools/Schools/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Logo,LogoHeight,LogoWidth,CurrentAcademicYearId")] School school)
        {
            if (id != school.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    //get object from database
                    var eSchool = _ctx.Schools.Find(id);
                    //update modelstate object with the appropriate values 
                    school.CreatedOn = eSchool.CreatedOn; //createdOn date for an edited object must not change. So maintain.
                    school.UpdatedOn = DateTime.Now;
                    //school.Timestamp = BitConverter.GetBytes(school.UpdatedOn.Ticks);
                    //update database object with modelstate object
                    _ctx.Entry(eSchool).CurrentValues.SetValues(school);
                    _ctx.Entry(eSchool).State = EntityState.Modified;
                    _ctx.Update(eSchool);
                    await _ctx.SaveChangesAsync();
                }
                catch(Exception ex)
                {

                //}
                //catch (DbUpdateConcurrencyException)
                //{
                    if (!SchoolExists(school.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CurrentAcademicYearId"] = new SelectList(_ctx.AcademicYears, "Id", "Name");
            return View(school);
        }

        // GET: Schools/Schools/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var school = await _ctx.Schools
                .SingleOrDefaultAsync(m => m.Id == id);
            if (school == null)
            {
                return NotFound();
            }

            return View(school);
        }

        // POST: Schools/Schools/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var school = await _ctx.Schools.SingleOrDefaultAsync(m => m.Id == id);
            _ctx.Schools.Remove(school);
            await _ctx.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SchoolExists(int id)
        {
            return _ctx.Schools.Any(e => e.Id == id);
        }
    }
}
