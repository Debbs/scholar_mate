﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ScholarMate.Infrastructures;
using ScholarMate.Model.Models;
using ScholarMate.Model.Models.Schools;

namespace ScholarMate.Web.Areas.Schools.Controllers
{
    [Area("Schools")]
    public class ClassesController : BaseController
    {

        public ClassesController(ScholarMateContext ctx) : 
            base(ctx)
        {
        }

        // GET: Schools/Classes
        public async Task<IActionResult> Index()
        {
            var scholarMateContext = _ctx.Classes.Include(a => a.Level).Include(a => a.School);
            return View(await scholarMateContext.ToListAsync());
        }

        // GET: Schools/Classes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @class = await _ctx.Classes
                .Include(a => a.Level)
                .Include(a => a.School)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (@class == null)
            {
                return NotFound();
            }

            return View(@class);
        }

        // GET: Schools/Classes/Create
        public IActionResult Create()
        {
            ViewData["LevelId"] = new SelectList(_ctx.Levels, "Id", "Name");
            return View();
        }

        // POST: Schools/Classes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("SchoolId,Name,LevelId")] Class @class)
        {
            if (ModelState.IsValid)
            {
                _ctx.Add(@class);
                await _ctx.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["LevelId"] = new SelectList(_ctx.Levels, "Id", "Name", @class.LevelId);
            return View(@class);
        }

        // GET: Schools/Classes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @class = await _ctx.Classes.SingleOrDefaultAsync(m => m.Id == id);
            if (@class == null)
            {
                return NotFound();
            }
            ViewData["LevelId"] = new SelectList(_ctx.Levels, "Id", "Name", @class.LevelId);
            return View(@class);
        }

        // POST: Schools/Classes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,SchoolId,Name,LevelId")] Class @class)
        {
            if (id != @class.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    //get object from database
                    var eClass = _ctx.Classes.Find(id);
                    //update database object with modelstate object
                    _ctx.Entry(eClass).CurrentValues.SetValues(@class);
                    _ctx.Entry(eClass).State = EntityState.Modified;
                    _ctx.Update(eClass);
                    await _ctx.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ClassExists(@class.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["LevelId"] = new SelectList(_ctx.Levels, "Id", "Name", @class.LevelId);
            return View(@class);
        }

        // GET: Schools/Classes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @class = await _ctx.Classes
                .Include(a => a.Level)
                .Include(a => a.School)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (@class == null)
            {
                return NotFound();
            }

            return View(@class);
        }

        // POST: Schools/Classes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var @class = await _ctx.Classes.SingleOrDefaultAsync(m => m.Id == id);
            _ctx.Classes.Remove(@class);
            await _ctx.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ClassExists(int id)
        {
            return _ctx.Classes.Any(e => e.Id == id);
        }
    }
}
