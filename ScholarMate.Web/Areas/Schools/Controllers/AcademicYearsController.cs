﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ScholarMate.Infrastructures;
using ScholarMate.Model.Models;
using ScholarMate.Model.Models.Schools;

namespace ScholarMate.Web.Areas.Schools.Controllers
{
    [Area("Schools")]
    public class AcademicYearsController : BaseController
    {

        public AcademicYearsController(ScholarMateContext ctx) : 
            base(ctx)
        {

        }

        // GET: Schools/AcademicYears
        public async Task<IActionResult> Index()
        {
            var scholarMateContext = _ctx.AcademicYears.Include(a => a.School);
            return View(await scholarMateContext.ToListAsync());
        }

        // GET: Schools/AcademicYears/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var academicYear = await _ctx.AcademicYears
                .Include(a => a.School)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (academicYear == null)
            {
                return NotFound();
            }

            return View(academicYear);
        }

        // GET: Schools/AcademicYears/Create
        public IActionResult Create()
        {
            ViewData["SchoolId"] = new SelectList(_ctx.Schools, "Id", "Name");
            return View();
        }

        // POST: Schools/AcademicYears/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("SchoolId,StartYear,EndYear,Name")] AcademicYear academicYear)
        {
            if (ModelState.IsValid)
            {
                academicYear.CreatedOn = DateTime.Now;
                academicYear.UpdatedOn = DateTime.Now;
                _ctx.Add(academicYear);
                await _ctx.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(academicYear);
        }

        // GET: Schools/AcademicYears/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var academicYear = await _ctx.AcademicYears.SingleOrDefaultAsync(m => m.Id == id);
            if (academicYear == null)
            {
                return NotFound();
            }
            return View(academicYear);
        }

        // POST: Schools/AcademicYears/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,SchoolId,StartYear,EndYear,Name")] AcademicYear academicYear)
        {
            if (id != academicYear.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    //get object from database
                    var eAcademicYear = _ctx.AcademicYears.Find(id);
                    //update modelstate object with the appropriate values 
                    academicYear.CreatedOn = eAcademicYear.CreatedOn; //createdOn date for an edited object must not change. So maintain.
                    academicYear.UpdatedOn = DateTime.Now;
                    //update database object with modelstate object
                    _ctx.Entry(eAcademicYear).CurrentValues.SetValues(academicYear);
                    _ctx.Entry(eAcademicYear).State = EntityState.Modified;
                    _ctx.Update(eAcademicYear);
                    await _ctx.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AcademicYearExists(academicYear.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(academicYear);
        }

        // GET: Schools/AcademicYears/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var academicYear = await _ctx.AcademicYears
                .Include(a => a.School)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (academicYear == null)
            {
                return NotFound();
            }

            return View(academicYear);
        }

        // POST: Schools/AcademicYears/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var academicYear = await _ctx.AcademicYears.SingleOrDefaultAsync(m => m.Id == id);
            _ctx.AcademicYears.Remove(academicYear);
            await _ctx.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AcademicYearExists(int id)
        {
            return _ctx.AcademicYears.Any(e => e.Id == id);
        }
    }
}
