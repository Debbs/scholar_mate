﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using ScholarMate.Model.Models;
using ScholarMate.Model.Models.Users;
using ScholarMate.Web.Infrastructures;
using ScholarMate.Web.Services;
using System.IO;

namespace ScholarMate.Web
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true);

            if (env.IsDevelopment())
            {
                // For more details on using the user secret store see https://go.microsoft.com/fwlink/?LinkID=532709
                //builder.AddUserSecrets<Startup>();
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            // Add framework services.
            services.AddDbContextPool<ScholarMateContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("ScholarMateContextConnection"), o =>
                {
                    o.EnableRetryOnFailure();
                    o.UseRowNumberForPaging();
                }));

            //services.AddDbContext<ScholarMateContext>(options =>
            //    options.UseSqlServer(Configuration["Data:ScholarMateContextConnection:ConnectionString"]));

            //Telling ASP.NET Core Identity that we are using long, not string
            services.AddIdentity<ApplicationUser, ApplicationRole>()
                .AddEntityFrameworkStores<ScholarMateContext>()
                .AddDefaultTokenProviders();

            //register our custom Claims transformation factory
            services.AddScoped<IUserClaimsPrincipalFactory<ApplicationUser>, AppClaimsPrincipalFactory>();

            // Add application services.
            services.AddTransient<IEmailSender, EmailSender>();

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseAuthentication();

            ////To register the claims transformer
            //app.UseClaimsTransformation(new ClaimsTransformationOptions
            //{
            //    Transformer = new ClaimsTransformer()
            //});

            app.UseStaticFiles();
            // Add support for node_modules but only during development **temporary**
            if (env.IsDevelopment())
            {
                app.UseStaticFiles(new StaticFileOptions()
                {
                    FileProvider = new PhysicalFileProvider(
                      Path.Combine(Directory.GetCurrentDirectory(), @"node_modules")),
                    RequestPath = new PathString("/vendor")
                });
            }

            app.UseMvc(routes =>
            {
                //routes.MapRoute(name: "Schools",
                //    template: "Schools//{controller}/{action}/{id?}",
                //    defaults: new { controller = "Home", action = "Index"},
                //    constraints: new { schoolId = @"^\d+$", id = @"^\d+$" });
                ////defaults: new { controller = "Home", action = "Index", area = "Schools" },

                routes.MapRoute(
                    name: "Schools",
                    template: "{area:exists}/{schoolId}/{controller}/{action}/{id?}",
                    defaults: new { controller = "Home", action = "Index"},
                    constraints: new { schoolId = @"^\d+$", id = @"^\d+$" }
                );

                //routes.MapRoute("areaRoute", "{area:exists}/{controller=Test}/{action=Index}/{id?}");

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
