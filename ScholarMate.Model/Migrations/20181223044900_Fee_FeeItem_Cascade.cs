﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ScholarMate.Model.Migrations
{
    public partial class Fee_FeeItem_Cascade : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FeeItemMaps_Fees_FeeId",
                table: "FeeItemMaps");

            migrationBuilder.AddForeignKey(
                name: "FK_FeeItemMaps_Fees_FeeId",
                table: "FeeItemMaps",
                column: "FeeId",
                principalTable: "Fees",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FeeItemMaps_Fees_FeeId",
                table: "FeeItemMaps");

            migrationBuilder.AddForeignKey(
                name: "FK_FeeItemMaps_Fees_FeeId",
                table: "FeeItemMaps",
                column: "FeeId",
                principalTable: "Fees",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
