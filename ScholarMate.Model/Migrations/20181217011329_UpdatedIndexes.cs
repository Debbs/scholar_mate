﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ScholarMate.Model.Migrations
{
    public partial class UpdatedIndexes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Students_SchoolId_UniqueId",
                table: "Students");

            migrationBuilder.DropIndex(
                name: "IX_Staffs_SchoolId_UniqueId",
                table: "Staffs");

            migrationBuilder.DropIndex(
                name: "IX_Fees_SchoolId",
                table: "Fees");

            migrationBuilder.DropIndex(
                name: "IX_Classes_SchoolId",
                table: "Classes");

            migrationBuilder.DropIndex(
                name: "IX_AcademicYears_SchoolId_StartYear_EndYear_Name",
                table: "AcademicYears");

            migrationBuilder.CreateIndex(
                name: "IX_Students_SchoolId_Id_UniqueId",
                table: "Students",
                columns: new[] { "SchoolId", "Id", "UniqueId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Staffs_SchoolId_Id_UniqueId",
                table: "Staffs",
                columns: new[] { "SchoolId", "Id", "UniqueId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Fees_Name",
                table: "Fees",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Fees_SchoolId_AcademicYearId_Id",
                table: "Fees",
                columns: new[] { "SchoolId", "AcademicYearId", "Id" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Classes_Name",
                table: "Classes",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Classes_SchoolId_LevelId_Id",
                table: "Classes",
                columns: new[] { "SchoolId", "LevelId", "Id" },
                unique: true,
                filter: "[LevelId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AcademicYears_Name",
                table: "AcademicYears",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AcademicYears_SchoolId_Id_StartYear_EndYear",
                table: "AcademicYears",
                columns: new[] { "SchoolId", "Id", "StartYear", "EndYear" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Students_SchoolId_Id_UniqueId",
                table: "Students");

            migrationBuilder.DropIndex(
                name: "IX_Staffs_SchoolId_Id_UniqueId",
                table: "Staffs");

            migrationBuilder.DropIndex(
                name: "IX_Fees_Name",
                table: "Fees");

            migrationBuilder.DropIndex(
                name: "IX_Fees_SchoolId_AcademicYearId_Id",
                table: "Fees");

            migrationBuilder.DropIndex(
                name: "IX_Classes_Name",
                table: "Classes");

            migrationBuilder.DropIndex(
                name: "IX_Classes_SchoolId_LevelId_Id",
                table: "Classes");

            migrationBuilder.DropIndex(
                name: "IX_AcademicYears_Name",
                table: "AcademicYears");

            migrationBuilder.DropIndex(
                name: "IX_AcademicYears_SchoolId_Id_StartYear_EndYear",
                table: "AcademicYears");

            migrationBuilder.CreateIndex(
                name: "IX_Students_SchoolId_UniqueId",
                table: "Students",
                columns: new[] { "SchoolId", "UniqueId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Staffs_SchoolId_UniqueId",
                table: "Staffs",
                columns: new[] { "SchoolId", "UniqueId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Fees_SchoolId",
                table: "Fees",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_Classes_SchoolId",
                table: "Classes",
                column: "SchoolId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AcademicYears_SchoolId_StartYear_EndYear_Name",
                table: "AcademicYears",
                columns: new[] { "SchoolId", "StartYear", "EndYear", "Name" },
                unique: true);
        }
    }
}
