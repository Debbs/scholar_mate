﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ScholarMate.Model.Migrations
{
    public partial class Add_CurrentAcademicYear : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CurrentAcademicYearId",
                table: "Schools",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Schools_CurrentAcademicYearId",
                table: "Schools",
                column: "CurrentAcademicYearId");

            migrationBuilder.AddForeignKey(
                name: "FK_Schools_AcademicYears_CurrentAcademicYearId",
                table: "Schools",
                column: "CurrentAcademicYearId",
                principalTable: "AcademicYears",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Schools_AcademicYears_CurrentAcademicYearId",
                table: "Schools");

            migrationBuilder.DropIndex(
                name: "IX_Schools_CurrentAcademicYearId",
                table: "Schools");

            migrationBuilder.DropColumn(
                name: "CurrentAcademicYearId",
                table: "Schools");
        }
    }
}
