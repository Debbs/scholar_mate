﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ScholarMate.Model.Migrations
{
    public partial class FeeItemMap_PrimaryKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropUniqueConstraint(
                name: "AK_FeeItemMaps_FeeId_FeeItemId_SchoolId",
                table: "FeeItemMaps");

            migrationBuilder.DropPrimaryKey(
                name: "PK_FeeItemMaps",
                table: "FeeItemMaps");

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "FeeItemMaps",
                type: "int",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_FeeItemMaps",
                table: "FeeItemMaps",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_FeeItemMaps_FeeId",
                table: "FeeItemMaps",
                column: "FeeId");

            migrationBuilder.CreateIndex(
                name: "IX_FeeItemMaps_SchoolId_Id_FeeId_FeeItemId",
                table: "FeeItemMaps",
                columns: new[] { "SchoolId", "Id", "FeeId", "FeeItemId" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_FeeItemMaps",
                table: "FeeItemMaps");

            migrationBuilder.DropIndex(
                name: "IX_FeeItemMaps_FeeId",
                table: "FeeItemMaps");

            migrationBuilder.DropIndex(
                name: "IX_FeeItemMaps_SchoolId_Id_FeeId_FeeItemId",
                table: "FeeItemMaps");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "FeeItemMaps");

            migrationBuilder.AddUniqueConstraint(
                name: "AK_FeeItemMaps_FeeId_FeeItemId_SchoolId",
                table: "FeeItemMaps",
                columns: new[] { "FeeId", "FeeItemId", "SchoolId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_FeeItemMaps",
                table: "FeeItemMaps",
                columns: new[] { "SchoolId", "FeeId", "FeeItemId" });
        }
    }
}
