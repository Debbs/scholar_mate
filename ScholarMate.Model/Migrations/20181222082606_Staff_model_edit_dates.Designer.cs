﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Storage.Internal;
using ScholarMate.Model.Models;
using System;

namespace ScholarMate.Model.Migrations
{
    [DbContext(typeof(ScholarMateContext))]
    [Migration("20181222082606_Staff_model_edit_dates")]
    partial class Staff_model_edit_dates
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.0.0-rtm-26452")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<long>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<long>("RoleId");

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<long>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<long>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<long>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<long>("UserId");

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<long>", b =>
                {
                    b.Property<long>("UserId");

                    b.Property<long>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<long>", b =>
                {
                    b.Property<long>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("ScholarMate.Model.Models.Fees.Fee", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AcademicYearId");

                    b.Property<DateTime>("CreatedOn");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<int>("SchoolId");

                    b.Property<byte[]>("Timestamp")
                        .IsConcurrencyToken()
                        .ValueGeneratedOnAddOrUpdate();

                    b.Property<DateTime>("UpdatedOn");

                    b.HasKey("Id");

                    b.HasIndex("AcademicYearId");

                    b.HasIndex("Name")
                        .IsUnique();

                    b.HasIndex("SchoolId", "AcademicYearId", "Id")
                        .IsUnique();

                    b.ToTable("Fees");
                });

            modelBuilder.Entity("ScholarMate.Model.Models.Fees.FeeItem", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<int>("SchoolId");

                    b.HasKey("Id");

                    b.HasIndex("SchoolId");

                    b.ToTable("FeeItems");
                });

            modelBuilder.Entity("ScholarMate.Model.Models.Fees.FeeItemMap", b =>
                {
                    b.Property<int>("SchoolId");

                    b.Property<int>("FeeId");

                    b.Property<int>("FeeItemId");

                    b.Property<decimal>("Amount")
                        .HasColumnType("decimal(18, 3)");

                    b.Property<DateTime>("CreatedOn");

                    b.Property<byte[]>("Timestamp")
                        .IsConcurrencyToken()
                        .ValueGeneratedOnAddOrUpdate();

                    b.Property<DateTime>("UpdatedOn");

                    b.HasKey("SchoolId", "FeeId", "FeeItemId");

                    b.HasAlternateKey("FeeId", "FeeItemId", "SchoolId");

                    b.HasIndex("FeeItemId");

                    b.ToTable("FeeItemMaps");
                });

            modelBuilder.Entity("ScholarMate.Model.Models.Schools.AcademicYear", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedOn");

                    b.Property<int>("EndYear");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<int>("SchoolId");

                    b.Property<int>("StartYear");

                    b.Property<byte[]>("Timestamp")
                        .IsConcurrencyToken()
                        .ValueGeneratedOnAddOrUpdate();

                    b.Property<DateTime>("UpdatedOn");

                    b.HasKey("Id");

                    b.HasIndex("Name")
                        .IsUnique();

                    b.HasIndex("SchoolId", "Id", "StartYear", "EndYear")
                        .IsUnique();

                    b.ToTable("AcademicYears");
                });

            modelBuilder.Entity("ScholarMate.Model.Models.Schools.Class", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("LevelId");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<int>("SchoolId");

                    b.Property<byte[]>("Timestamp")
                        .IsConcurrencyToken()
                        .ValueGeneratedOnAddOrUpdate();

                    b.HasKey("Id");

                    b.HasIndex("LevelId");

                    b.HasIndex("Name")
                        .IsUnique();

                    b.HasIndex("SchoolId", "LevelId", "Id")
                        .IsUnique()
                        .HasFilter("[LevelId] IS NOT NULL");

                    b.ToTable("Classes");
                });

            modelBuilder.Entity("ScholarMate.Model.Models.Schools.Level", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<int>("SchoolId");

                    b.Property<byte[]>("Timestamp")
                        .IsConcurrencyToken()
                        .ValueGeneratedOnAddOrUpdate();

                    b.HasKey("Id");

                    b.HasIndex("SchoolId");

                    b.ToTable("Levels");
                });

            modelBuilder.Entity("ScholarMate.Model.Models.Schools.School", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedOn");

                    b.Property<int?>("CurrentAcademicYearId");

                    b.Property<byte[]>("Logo");

                    b.Property<int>("LogoHeight");

                    b.Property<int>("LogoWidth");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<byte[]>("Timestamp")
                        .IsConcurrencyToken()
                        .ValueGeneratedOnAddOrUpdate();

                    b.Property<DateTime>("UpdatedOn");

                    b.HasKey("Id");

                    b.HasIndex("CurrentAcademicYearId");

                    b.ToTable("Schools");
                });

            modelBuilder.Entity("ScholarMate.Model.Models.Schools.Staff", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedOn");

                    b.Property<DateTime?>("DateOfBirth");

                    b.Property<DateTime>("DateOfRecruitment");

                    b.Property<bool>("IsTeaching");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<int>("SchoolId");

                    b.Property<byte[]>("Timestamp")
                        .IsConcurrencyToken()
                        .ValueGeneratedOnAddOrUpdate();

                    b.Property<string>("UniqueId")
                        .IsRequired()
                        .HasMaxLength(150);

                    b.Property<DateTime>("UpdatedOn");

                    b.HasKey("Id");

                    b.HasIndex("SchoolId", "Id", "UniqueId")
                        .IsUnique();

                    b.ToTable("Staffs");
                });

            modelBuilder.Entity("ScholarMate.Model.Models.Schools.Student", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ClassId");

                    b.Property<DateTime>("CreatedOn");

                    b.Property<DateTime>("DateOfBirth");

                    b.Property<DateTime?>("DateOfCompletion");

                    b.Property<DateTime>("DateOfRegistration");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<int>("SchoolId");

                    b.Property<byte[]>("Timestamp")
                        .IsConcurrencyToken()
                        .ValueGeneratedOnAddOrUpdate();

                    b.Property<string>("UniqueId")
                        .IsRequired()
                        .HasMaxLength(150);

                    b.Property<DateTime>("UpdatedOn");

                    b.HasKey("Id");

                    b.HasIndex("ClassId");

                    b.HasIndex("SchoolId", "Id", "UniqueId")
                        .IsUnique();

                    b.ToTable("Students");
                });

            modelBuilder.Entity("ScholarMate.Model.Models.Schools.StudentClassMap", b =>
                {
                    b.Property<int>("StudentId");

                    b.Property<int>("ClassId");

                    b.Property<int>("AcademicYearId");

                    b.Property<DateTime>("CreatedOn");

                    b.Property<byte[]>("Timestamp")
                        .IsConcurrencyToken()
                        .ValueGeneratedOnAddOrUpdate();

                    b.Property<DateTime>("UpdatedOn");

                    b.HasKey("StudentId", "ClassId", "AcademicYearId");

                    b.HasAlternateKey("AcademicYearId", "ClassId", "StudentId");

                    b.HasIndex("ClassId");

                    b.ToTable("StudentClassMap");
                });

            modelBuilder.Entity("ScholarMate.Model.Models.Users.ApplicationRole", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("Name")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex")
                        .HasFilter("[NormalizedName] IS NOT NULL");

                    b.ToTable("Roles");
                });

            modelBuilder.Entity("ScholarMate.Model.Models.Users.ApplicationUser", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Email")
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<int>("SchoolId");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex")
                        .HasFilter("[NormalizedUserName] IS NOT NULL");

                    b.HasIndex("SchoolId");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<long>", b =>
                {
                    b.HasOne("ScholarMate.Model.Models.Users.ApplicationRole")
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<long>", b =>
                {
                    b.HasOne("ScholarMate.Model.Models.Users.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<long>", b =>
                {
                    b.HasOne("ScholarMate.Model.Models.Users.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<long>", b =>
                {
                    b.HasOne("ScholarMate.Model.Models.Users.ApplicationRole")
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("ScholarMate.Model.Models.Users.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<long>", b =>
                {
                    b.HasOne("ScholarMate.Model.Models.Users.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("ScholarMate.Model.Models.Fees.Fee", b =>
                {
                    b.HasOne("ScholarMate.Model.Models.Schools.AcademicYear", "AcademicYear")
                        .WithMany()
                        .HasForeignKey("AcademicYearId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("ScholarMate.Model.Models.Schools.School", "School")
                        .WithMany("Fees")
                        .HasForeignKey("SchoolId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ScholarMate.Model.Models.Fees.FeeItem", b =>
                {
                    b.HasOne("ScholarMate.Model.Models.Schools.School", "School")
                        .WithMany()
                        .HasForeignKey("SchoolId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("ScholarMate.Model.Models.Fees.FeeItemMap", b =>
                {
                    b.HasOne("ScholarMate.Model.Models.Fees.Fee", "Fee")
                        .WithMany("FeeItems")
                        .HasForeignKey("FeeId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("ScholarMate.Model.Models.Fees.FeeItem", "FeeItem")
                        .WithMany("FeeItems")
                        .HasForeignKey("FeeItemId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("ScholarMate.Model.Models.Schools.School", "School")
                        .WithMany()
                        .HasForeignKey("SchoolId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("ScholarMate.Model.Models.Schools.AcademicYear", b =>
                {
                    b.HasOne("ScholarMate.Model.Models.Schools.School", "School")
                        .WithMany("AcademicYears")
                        .HasForeignKey("SchoolId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ScholarMate.Model.Models.Schools.Class", b =>
                {
                    b.HasOne("ScholarMate.Model.Models.Schools.Level", "Level")
                        .WithMany("Classes")
                        .HasForeignKey("LevelId");

                    b.HasOne("ScholarMate.Model.Models.Schools.School", "School")
                        .WithMany("Classes")
                        .HasForeignKey("SchoolId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ScholarMate.Model.Models.Schools.Level", b =>
                {
                    b.HasOne("ScholarMate.Model.Models.Schools.School", "School")
                        .WithMany("Levels")
                        .HasForeignKey("SchoolId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ScholarMate.Model.Models.Schools.School", b =>
                {
                    b.HasOne("ScholarMate.Model.Models.Schools.AcademicYear", "CurrentAcademicYear")
                        .WithMany()
                        .HasForeignKey("CurrentAcademicYearId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("ScholarMate.Model.Models.Schools.Staff", b =>
                {
                    b.HasOne("ScholarMate.Model.Models.Schools.School", "School")
                        .WithMany("Staffs")
                        .HasForeignKey("SchoolId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ScholarMate.Model.Models.Schools.Student", b =>
                {
                    b.HasOne("ScholarMate.Model.Models.Schools.Class", "Class")
                        .WithMany()
                        .HasForeignKey("ClassId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("ScholarMate.Model.Models.Schools.School", "School")
                        .WithMany("Students")
                        .HasForeignKey("SchoolId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ScholarMate.Model.Models.Schools.StudentClassMap", b =>
                {
                    b.HasOne("ScholarMate.Model.Models.Schools.AcademicYear", "AcademicYear")
                        .WithMany()
                        .HasForeignKey("AcademicYearId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("ScholarMate.Model.Models.Schools.Class", "Class")
                        .WithMany("StudentClasses")
                        .HasForeignKey("ClassId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("ScholarMate.Model.Models.Schools.Student", "Student")
                        .WithMany("StudentClasses")
                        .HasForeignKey("StudentId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("ScholarMate.Model.Models.Users.ApplicationUser", b =>
                {
                    b.HasOne("ScholarMate.Model.Models.Schools.School", "School")
                        .WithMany()
                        .HasForeignKey("SchoolId")
                        .OnDelete(DeleteBehavior.Restrict);
                });
#pragma warning restore 612, 618
        }
    }
}
