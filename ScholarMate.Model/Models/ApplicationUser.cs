﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using ScholarMate.Model.Models.Schools;
using Microsoft.AspNetCore.Identity;

namespace ScholarMate.Model.Models.Users
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser<long>
    {
        //public ApplicationUser()
        //{
        //    CanBeDeleted = true;
        //}
        [Required]
        [DisplayName("School")]
        public int SchoolId { get; set; }

        [ForeignKey("SchoolId")]
        public School School { get; set; }

        [Required(ErrorMessage = "Enter name"), DisplayName("Name"), JsonProperty]
        [MaxLength(255, ErrorMessage = "Name cannot be more than {0} characters")]
        public string Name { get; set; }

        //[NotMapped]
        //public bool IsRoot { get; set; }
    }
}