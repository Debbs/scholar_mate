﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNetCore.Identity;
using Microsoft.Owin;

namespace ScholarMate.Model.Models.Users
{
    public class RoleManager : RoleManager<IdentityRole>
    {
        public RoleManager(RoleStore<IdentityRole> store)
            : base(store)
        {
        }
        public static RoleManager Create(
        IdentityFactoryOptions<RoleManager> options,
        IOwinContext context)
        {
            return new RoleManager(new
            RoleStore<IdentityRole>(context.Get<ScholarMateContext>()));
        }
    }
}
