﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace ScholarMate.Model.Models.Users
{
    public class ApplicationRole : IdentityRole<long>
    {
        [Required, MaxLength(255)]
        public string Description { get; set; }
    }
}
