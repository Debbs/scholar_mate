﻿using Newtonsoft.Json;
using ScholarMate.Model.Models.Base;
using ScholarMate.Model.Models.Schools;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ScholarMate.Model.Models.Fees
{
    public class FeeItemMap : EntityWithTypedId<int>, IWasCreatedOn, IWasUpdatedOn
    {
        [Required(ErrorMessage = "Select school"), DisplayName("School")]
        public int SchoolId { get; set; }

        [ForeignKey("SchoolId"), JsonProperty]
        public School School { get; set; }

        [Required]
        public int FeeId { get; set; }

        [ForeignKey("FeeId"), JsonProperty]
        public virtual Fee Fee { get; set; }

        [Required]
        public int FeeItemId { get; set; }

        [ForeignKey("FeeItemId"), JsonProperty]
        public virtual FeeItem FeeItem { get; set; }


        [DisplayName("Amount"), JsonProperty]
        public decimal Amount { get; set; }

        [Description("Index used when sorting.")]
        [Display(Prompt = "sort index")]
        public int? SortIndex { get; set; }

        [Timestamp]
        public byte[] Timestamp { get; set; }

        [DisplayName("Created On"), JsonProperty]
        public DateTime CreatedOn { get; set; }

        [DisplayName("Last Updated On"), JsonProperty]
        public DateTime UpdatedOn { get; set; }

    }
}
