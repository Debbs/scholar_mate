﻿using Newtonsoft.Json;
using ScholarMate.Model.Models.Base;
using ScholarMate.Model.Models.Schools;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ScholarMate.Model.Models.Fees
{
    public class Fee : EntityWithTypedId<int>, IWasCreatedOn, IWasUpdatedOn
    {
        [DomainSignature]
        [Required(ErrorMessage = "Select school"), DisplayName("School")]
        public int SchoolId { get; set; }

        [ForeignKey("SchoolId"), JsonProperty]
        public School School { get; set; }

        [DomainSignature]
        [Required, DisplayName("Academic Year")]
        public int AcademicYearId { get; set; }

        [ForeignKey("AcademicYearId"), JsonProperty]
        public AcademicYear AcademicYear { get; set; }

        [Required(ErrorMessage = "Enter name for fee"), DisplayName("Name"), JsonProperty]
        [MaxLength(255, ErrorMessage = "Name cannot be more than {0} characters")]
        public string Name { get; set; }

        [Timestamp]
        public byte[] Timestamp { get; set; }

        [DisplayName("Created On"), JsonProperty]
        public DateTime CreatedOn { get; set; }

        [DisplayName("Updated On"), JsonProperty]
        public DateTime UpdatedOn { get; set; }

        #region InverseProperties
        [InverseProperty("Fee"), JsonProperty(IsReference = false)]
        public ICollection<FeeItemMap> FeeItems { get; set; }
        #endregion

        public Fee()
        {
            FeeItems = new HashSet<FeeItemMap>();
        }

    }
}
