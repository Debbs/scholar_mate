﻿using Newtonsoft.Json;
using ScholarMate.Model.Models.Base;
using ScholarMate.Model.Models.Schools;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ScholarMate.Model.Models.Fees
{
    public class FeeItem : EntityWithTypedId<int>
    {
        [DomainSignature]
        [Required(ErrorMessage = "Select school"), DisplayName("School")]
        //[Index("IX_school_fee_item", IsUnique = true, Order = 1)]
        public int SchoolId { get; set; }

        [ForeignKey("SchoolId"), JsonProperty]
        public School School { get; set; }

        //[Index("IX_school_fee_item", IsUnique = true, Order = 2)]
        [Required(ErrorMessage = "Enter name for fee item"), DisplayName("Name"), JsonProperty]
        [MaxLength(255, ErrorMessage = "Name cannot be more than {0} characters")]
        public string Name { get; set; }

        [Description("Index used when sorting.")]
        [Display(Prompt ="sort index")]
        public int? SortIndex { get; set; }

        [DisplayName("Is Active")]
        public bool Active { get; set; }

        #region InverseProperties
        [InverseProperty("FeeItem"), JsonProperty(IsReference = false)]
        public ICollection<FeeItemMap> FeeItems { get; set; }
        #endregion

        public FeeItem()
        {
            FeeItems = new HashSet<FeeItemMap>();
        }

    }
}
