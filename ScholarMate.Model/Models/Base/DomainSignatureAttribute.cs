﻿using System;

namespace ScholarMate.Model.Models.Base
{
    /// <summary>
    ///     Facilitates indicating which property(s) describe the unique signature of an
    ///     entity. See Entity.GetTypeSpecificSignatureProperties() for when this is leveraged.
    /// </summary>
    /// <remarks>
    ///     This is intended for use with <see cref="Entity" />.
    /// </remarks>
    [Serializable]
    [AttributeUsage(AttributeTargets.Property)]
    public class DomainSignatureAttribute : Attribute
    {
    }
}