﻿using System.Collections.Generic;
using System.Reflection;

namespace ScholarMate.Model.Models.Base
{
    /// <summary>
    ///     This serves as a base interface for <see cref="EntityWithTypedId{TId}" />.
    ///     Also provides a simple means to develop your own base entity.
    /// </summary>
    public interface IEntityWithTypedId<TId>
    {
        TId Id { get; }

        IEnumerable<PropertyInfo> GetSignatureProperties();

        bool IsTransient();
    }
}