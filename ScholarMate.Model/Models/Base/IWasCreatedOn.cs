﻿using System;
using System.ComponentModel;

namespace ScholarMate.Model.Models.Base
{
    public interface IWasCreatedOn
    {
        [DisplayName("Created On")]
        DateTime CreatedOn { get; set; }
    }
}