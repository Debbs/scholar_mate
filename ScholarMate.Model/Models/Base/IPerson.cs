﻿using System;

namespace ScholarMate.Model.Models.Base
{
    public interface IPerson<TId>
    {
        TId Id { get; }

        int SchoolId { get; set; }

        string Name { get; set; }

        string UniqueId { get; set; }

        DateTime CreatedOn { get; set; }

        DateTime UpdatedOn { get; set; }

    }
}