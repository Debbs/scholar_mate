using System;
using System.ComponentModel;

namespace ScholarMate.Model.Models.Base
{
    public interface IWasUpdatedOn
    {
        [DisplayName("Updated On")]
        DateTime UpdatedOn { get; set; }
    }
}