namespace ScholarMate.Model.Models
{
    using Fees;
    using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore;
    using ScholarMate.Model.Models.Users;
    using Schools;
    using System.Linq;

    public class ScholarMateContext : IdentityDbContext<ApplicationUser, ApplicationRole, long>
    {
        // Your context has been configured to use a 'ScholarMateContext' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'ScholarMate.Model.Models.ScholarMateContext' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'ScholarMateContext' 
        // connection string in the application configuration file.
        public ScholarMateContext(DbContextOptions<ScholarMateContext> options)
            : base(options)
        {
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
        public virtual DbSet<School> Schools { get; set; }
        public virtual DbSet<Student> Students { get; set; }
        public virtual DbSet<AcademicYear> AcademicYears { get; set; }
        public virtual DbSet<Class> Classes { get; set; }
        public virtual DbSet<Fee> Fees { get; set; }
        public virtual DbSet<FeeItem> FeeItems { get; set; }
        public virtual DbSet<FeeItemMap> FeeItemMaps { get; set; }
        public virtual DbSet<Staff>   Staffs { get; set; }
        public virtual DbSet<Level> Levels { get; set; }
        //public virtual DbSet<TreatmentProtocol> TreatmentProtocols { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            //builder.Conventions.Remove<PluralizingTableNameConvention>();
            //builder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            foreach (var property in builder.Model.GetEntityTypes().SelectMany(t => t.GetProperties()).Where(p => p.ClrType == typeof(decimal)))
            {
                property.Relational().ColumnType = "decimal(18, 3)";
            }

            //change the name of the Indentity tables
            builder.Entity<ApplicationUser>()
                .ToTable("Users");

            builder.Entity<ApplicationRole>()
                .ToTable("Roles");

            foreach (var relationship in builder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }

            builder.Entity<School>()
                .HasMany(s => s.Students)
                .WithOne(x => x.School)
                .HasForeignKey(s => s.SchoolId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<School>()
                .HasMany(s => s.AcademicYears)
                .WithOne(s => s.School)
                .HasForeignKey(s => s.SchoolId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<School>()
                .HasMany(s => s.Classes)
                .WithOne(s => s.School)
                .HasForeignKey(s => s.SchoolId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<School>()
                .HasMany(s => s.Levels)
                .WithOne(s => s.School)
                .HasForeignKey(s => s.SchoolId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<School>()
                .HasMany(s => s.Fees)
                .WithOne(s => s.School)
                .HasForeignKey(s => s.SchoolId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<School>()
                .HasMany(s => s.Staffs)
                .WithOne(s => s.School)
                .HasForeignKey(s => s.SchoolId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<Level>()
                .HasMany(s => s.Classes)
                .WithOne(s => s.Level)
                .HasForeignKey(s => s.LevelId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder.Entity<Fee>()
                .HasMany(s => s.FeeItems)
                .WithOne(s => s.Fee)
                .HasForeignKey(s => s.FeeId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<StudentClassMap>()
            .HasKey(c => new { c.StudentId, c.ClassId, c.AcademicYearId });

            //builder.Entity<FeeItemMap>()
            //.HasKey(c => new { c.SchoolId, c.FeeId, c.FeeItemId });

            builder.Entity<Fee>()
            .HasIndex(c => new { c.SchoolId, c.AcademicYearId, c.Id})
            .IsUnique();

            builder.Entity<Fee>()
            .HasIndex(c => c.Name)
            .IsUnique();

             builder.Entity<AcademicYear>()
            .HasIndex(c => new { c.SchoolId, c.Id, c.StartYear, c.EndYear})
            .IsUnique();

            builder.Entity<AcademicYear>()
            .HasIndex(c => c.Name)
            .IsUnique();

           builder.Entity<Class>()
            .HasIndex(c => new { c.SchoolId, c.LevelId, c.Id})
            .IsUnique();

            builder.Entity<Class>()
            .HasIndex(c => c.Name)
            .IsUnique();

            builder.Entity<Staff>()
            .HasIndex(c => new { c.SchoolId, c.Id, c.UniqueId })
            .IsUnique();

            builder.Entity<Student>()
            .HasIndex(c => new { c.SchoolId, c.Id, c.UniqueId})
            .IsUnique();

            builder.Entity<FeeItem>()
           .Property(c => c.SortIndex);

            builder.Entity<FeeItem>()
           .Property(c => c.Active)
           .HasDefaultValue(value: true);

             builder.Entity<FeeItemMap>()
            .HasIndex(c => new { c.SchoolId, c.Id, c.FeeId, c.FeeItemId})
            .IsUnique();


           //builder.Entity<LevelClassMap>()
            //.HasKey(c => new { c.SchoolId, c.LevelId, c.ClassId });
            //builder.Entity<TreatmentProtocol>()
            //.HasKey(c => new { c.TreatmentProtocolName, c.DiagnosisTypeId });
        }

    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}