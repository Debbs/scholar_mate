﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ScholarMate.Model.Models.Base;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ScholarMate.Model.Models.Schools
{

    public class Class : EntityWithTypedId<int>//, IWasCreatedOn, IWasUpdatedOn
    {
        [DomainSignature]
        [Required(ErrorMessage = "Select school"), DisplayName("School")]
        //[Index("IX_school_class", IsUnique = true, Order = 1)]
        public int SchoolId { get; set; }

        [ForeignKey("SchoolId"), JsonProperty]
        public School School { get; set; }

        [Required(ErrorMessage = "Enter name"), DisplayName("Name"), JsonProperty]
        [MaxLength(255, ErrorMessage = "Name cannot be more than {0} characters")]
        public string Name { get; set; }

        [DisplayName("Level")]
        public int? LevelId { get; set; }

        [ForeignKey("LevelId"), JsonProperty]
        public Level Level { get; set; }

        [Timestamp]
        public byte[] Timestamp { get; set; }

        //[DisplayName("Created On"), JsonProperty]
        //public DateTime CreatedOn { get; set; }

        //[DisplayName("Last Updated On"), JsonProperty]
        //public DateTime UpdatedOn { get; set; }

        #region InverseProperties
        [InverseProperty("Class"), JsonProperty(IsReference = false)]
        public ICollection<StudentClassMap> StudentClasses { get; set; }
        #endregion

        public Class()
        {
            StudentClasses = new HashSet<StudentClassMap>();
        }
    }
}