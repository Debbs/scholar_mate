﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ScholarMate.Model.Models.Base;
using Newtonsoft.Json;
using System;
using ScholarMate.Model.Models.Fees;

namespace ScholarMate.Model.Models.Schools
{
    public sealed class School : EntityWithTypedId<int>, IWasCreatedOn, IWasUpdatedOn
    {
        [Column]
        public override int Id { get; set; }

        [DomainSignature, JsonProperty]
        [Required, MaxLength(255, ErrorMessage = "Name cannot be more than {0} characters")]
        [DisplayName("Name")]
        public string Name { get; set; }

        [JsonProperty]
        public byte[] Logo { get; set; }

        [JsonProperty]
        public int LogoHeight { get; set; }

        [JsonProperty]
        public int LogoWidth { get; set; }

        [DisplayName("Current Academic Year")]
        public int? CurrentAcademicYearId { get; set; }

        [ForeignKey("CurrentAcademicYearId"), JsonProperty]
        public AcademicYear CurrentAcademicYear { get; set; }

        [Timestamp]
        public byte[] Timestamp { get; set; }

        [DisplayName("Created On"), JsonProperty]
        public DateTime CreatedOn { get; set; }

        [DisplayName("Last Updated On"), JsonProperty]
        public DateTime UpdatedOn { get; set; }

        #region InverseProperties
        [InverseProperty("School"), JsonProperty(IsReference = false)]
        public ICollection<Class> Classes { get; set; }

        [InverseProperty("School"), JsonProperty(IsReference = false)]
        public ICollection<AcademicYear> AcademicYears { get; set; }

        [InverseProperty("School"), JsonProperty(IsReference = false)]
        public ICollection<Student> Students { get; set; }

        [InverseProperty("School"), JsonProperty(IsReference = false)]
        public ICollection<Level> Levels { get; set; }

        [InverseProperty("School"), JsonProperty(IsReference = false)]
        public ICollection<Fee> Fees { get; set; }

        [InverseProperty("School"), JsonProperty(IsReference = false)]
        public ICollection<Staff> Staffs { get; set; }
        #endregion

        public School()
        {
            Classes = new HashSet<Class>();
            AcademicYears = new HashSet<AcademicYear>();
            Students = new HashSet<Student>();
            Levels = new HashSet<Level>();
            Fees = new HashSet<Fee>();
            Staffs = new HashSet<Staff>();
        }

    }
}