﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ScholarMate.Model.Models.Base;
using Newtonsoft.Json;

namespace ScholarMate.Model.Models.Schools
{

    public class Staff : EntityWithTypedId<int>, IPerson<int>, IWasCreatedOn, IWasUpdatedOn
    {
        [DomainSignature]
        [Required(ErrorMessage = "Select school"), DisplayName("School")]
        //[Index("IX_school_staff", IsUnique = true, Order = 1)]
        public int SchoolId { get; set; }

        [ForeignKey("SchoolId"), JsonProperty]
        public School School { get; set; }

        [DomainSignature, JsonProperty]
        [DisplayName("Staff ID")]
        [Required, MaxLength(150, ErrorMessage = "Staff ID cannot be more than {0} characters")]
        public string UniqueId { get; set; }

        [DisplayName("Date of Birth"), JsonProperty]
        public DateTime? DateOfBirth { get; set; }

        [DisplayName("Date Of Recruitment"), JsonProperty]
        public DateTime DateOfRecruitment { get; set; }

        [Required(ErrorMessage = "Enter name"), DisplayName("Name"), JsonProperty]
        [MaxLength(255, ErrorMessage = "Name cannot be more than {0} characters")]
        public string Name { get; set; }

        [JsonProperty]
        public bool IsTeaching { get; set; }

        [Timestamp]
        public byte[] Timestamp { get; set; }

        [DisplayName("Created On"), JsonProperty]
        public DateTime CreatedOn { get; set; }

        [DisplayName("Updated On"), JsonProperty]
        public DateTime UpdatedOn { get; set; }

        #region InverseProperties

        #endregion

    }
}