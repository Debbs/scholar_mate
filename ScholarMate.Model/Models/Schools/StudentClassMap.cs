﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ScholarMate.Model.Models.Base;
using Newtonsoft.Json;

namespace ScholarMate.Model.Models.Schools
{

    public class StudentClassMap : /*EntityWithTypedId<int>,*/ IWasCreatedOn, IWasUpdatedOn
    {
        [Key]
        [Column(Order =1)]
        [Required]
        //[Index("IX_student_class", IsUnique = true, Order = 1)]
        public int StudentId { get; set; }

        [ForeignKey("StudentId"), JsonProperty]
        public Student Student { get; set; }

        [Key]
        [Column(Order = 2)]
        [Required]
        //[Index("IX_student_class", IsUnique = true, Order = 2)]
        public int ClassId { get; set; }

        [ForeignKey("ClassId"), JsonProperty]
        public Class Class { get; set; }

        [Key]
        [Column(Order = 3)]
        [Required]
        //[Index("IX_student_class", IsUnique = true, Order = 3)]
        public int AcademicYearId { get; set; }

        [ForeignKey("AcademicYearId"), JsonProperty]
        public AcademicYear AcademicYear { get; set; }


        [Timestamp]
        public byte[] Timestamp { get; set; }

        [DisplayName("Created On"), JsonProperty]
        public DateTime CreatedOn { get; set; }

        [DisplayName("Last Updated On"), JsonProperty]
        public DateTime UpdatedOn { get; set; }
    }
}