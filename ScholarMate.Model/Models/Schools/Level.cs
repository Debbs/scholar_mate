﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ScholarMate.Model.Models.Base;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ScholarMate.Model.Models.Schools
{

    public class Level : EntityWithTypedId<int>
    {
        [DomainSignature]
        [Required(ErrorMessage = "Select level"), DisplayName("Level")]
        public int SchoolId { get; set; }

        [ForeignKey("SchoolId"), JsonProperty]
        public School School { get; set; }

        [Required(ErrorMessage = "Enter name"), DisplayName("Name"), JsonProperty]
        [MaxLength(255, ErrorMessage = "Name cannot be more than {0} characters")]
        public string Name { get; set; }


        [Timestamp]
        public byte[] Timestamp { get; set; }

        #region InverseProperties
        [InverseProperty("Level"), JsonProperty(IsReference = false)]
        public ICollection<Class> Classes { get; set; }
        #endregion

        public Level()
        {
            Classes = new HashSet<Class>();
        }
    }
}