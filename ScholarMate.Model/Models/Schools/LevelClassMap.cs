﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ScholarMate.Model.Models.Base;
using Newtonsoft.Json;

namespace ScholarMate.Model.Models.Schools
{

    public class LevelClassMap : IWasCreatedOn, IWasUpdatedOn
    {
        [Key]
        [Column(Order =1)]
        [Required]
        public int SchoolId { get; set; }

        [ForeignKey("StudentId"), JsonProperty]
        public School School { get; set; }

        [Key]
        [Column(Order =2)]
        [Required]
        public int LevelId { get; set; }

        [ForeignKey("LevelId"), JsonProperty]
        public Level Level { get; set; }

        [Key]
        [Column(Order = 3)]
        [Required]
        public int ClassId { get; set; }

        [ForeignKey("ClassId"), JsonProperty]
        public Class Class { get; set; }

        [Timestamp]
        public byte[] Timestamp { get; set; }

        [DisplayName("Created On"), JsonProperty]
        public DateTime CreatedOn { get; set; }

        [DisplayName("Updated On"), JsonProperty]
        public DateTime UpdatedOn { get; set; }
    }
}