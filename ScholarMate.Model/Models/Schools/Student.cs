﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ScholarMate.Model.Models.Base;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ScholarMate.Model.Models.Schools
{

    public class Student : EntityWithTypedId<int>, IPerson<int>, IWasCreatedOn, IWasUpdatedOn
    {
        [DomainSignature]
        [Required(ErrorMessage = "Select school"), DisplayName("School")]
        //[Index("IX_school_student", IsUnique = true, Order = 1)]
        public int SchoolId { get; set; }

        [ForeignKey("SchoolId"), JsonProperty]
        public School School { get; set; }

        [DomainSignature, JsonProperty]
        [DisplayName("Student ID")]
        [Required, MaxLength(150, ErrorMessage = "Student ID cannot be more than {0} characters")]
        //[Index("IX_school_student", IsUnique = true, Order = 2)]
        public string UniqueId { get; set; }

        [DisplayName("Date of Birth"), JsonProperty]
        public DateTime DateOfBirth { get; set; }

        [DisplayName("Date Of Registration"), JsonProperty]
        public DateTime DateOfRegistration { get; set; }

        [DisplayName("Date Of Completion"), JsonProperty]
        public DateTime? DateOfCompletion { get; set; }

        [Required(ErrorMessage = "Enter name"), DisplayName("Name"), JsonProperty]
        [MaxLength(255, ErrorMessage = "Name cannot be more than {0} characters")]
        public string Name { get; set; }

        [DomainSignature]
        [Required(ErrorMessage = "Select starting class"), DisplayName("Starting Class")]
        public int ClassId { get; set; }

        [ForeignKey("ClassId"), JsonProperty]
        public Class Class { get; set; }

        [Timestamp]
        public byte[] Timestamp { get; set; }

        [DisplayName("Created On"), JsonProperty]
        public DateTime CreatedOn { get; set; }

        [DisplayName("Last Updated On"), JsonProperty]
        public DateTime UpdatedOn { get; set; }

        #region InverseProperties
        [InverseProperty("Student"), JsonProperty(IsReference = false)]
        public ICollection<StudentClassMap> StudentClasses { get; set; }
        #endregion

        public Student()
        {
            StudentClasses = new HashSet<StudentClassMap>();
        }

    }
}