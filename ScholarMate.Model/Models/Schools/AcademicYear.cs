﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ScholarMate.Model.Models.Base;
using Newtonsoft.Json;

namespace ScholarMate.Model.Models.Schools
{

    public class AcademicYear : EntityWithTypedId<int>, IWasCreatedOn, IWasUpdatedOn
    {
        [DomainSignature]
        [Required(ErrorMessage = "Select school"), DisplayName("School")]
        //[Index("IX_school_academicyear", IsUnique = true, Order = 1)]
        public int SchoolId { get; set; }

        [ForeignKey("SchoolId"), JsonProperty]
        public School School { get; set; }

        [Required(ErrorMessage = "Select starting year for Academic Year"), DisplayName("Starting year")]
        //[Index("IX_school_academicyear", IsUnique = true, Order = 2)]
        public int StartYear { get; set; }

        [Required(ErrorMessage = "Select ending year for Academic Year"), DisplayName("Ending year")]
        //[Index("IX_school_academicyear", IsUnique = true, Order = 3)]
        public int EndYear { get; set; }

        [Required(ErrorMessage = "Enter name for Academic Year"), DisplayName("Academic Year"), JsonProperty]
        [MaxLength(200, ErrorMessage = "Academic Year name cannot be more than {0} characters")]
        //[Index("IX_school_academicyear", IsUnique = true, Order = 4)]
        public string Name { get; set; }


        [Timestamp]
        public byte[] Timestamp { get; set; }

        [DisplayName("Created On"), JsonProperty]
        public DateTime CreatedOn { get; set; }

        [DisplayName("Last Updated On"), JsonProperty]
        public DateTime UpdatedOn { get; set; }
    }
}