namespace ScholarMate.Core.Models
{
    using Fees;
    using Schools;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration.Conventions;
    using System.Linq;

    public class ScholarMateContext : DbContext
    {
        // Your context has been configured to use a 'ScholarMateContext' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'ScholarMate.Core.Models.ScholarMateContext' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'ScholarMateContext' 
        // connection string in the application configuration file.
        public ScholarMateContext()
            : base("name=ScholarMateContext")
        {
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
        public virtual DbSet<School> Schools { get; set; }
        public virtual DbSet<Student> Students { get; set; }
        public virtual DbSet<AcademicYear> AcademicYears { get; set; }
        public virtual DbSet<Class> Classes { get; set; }
        public virtual DbSet<Fee> Fees { get; set; }
        public virtual DbSet<FeeItem> BillItems { get; set; }
        public virtual DbSet<FeeItemMap> FeeBillItems { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            modelBuilder.Entity<School>()
                .HasMany(s => s.Students)
                .WithRequired(s => s.School)
                .HasForeignKey(s => s.SchoolId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<School>()
                .HasMany(s => s.AcademicYears)
                .WithRequired(s => s.School)
                .HasForeignKey(s => s.SchoolId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<School>()
                .HasMany(s => s.Classes)
                .WithRequired(s => s.School)
                .HasForeignKey(s => s.SchoolId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<FeeItemMap>().Property(x => x.Amount)
                .HasPrecision(18, 3);

            //modelBuilder.Entity<Class>()
            //    .HasMany(s => s.StudentClasses)
            //    .WithRequired(s => s.Class)
            //    .HasForeignKey(s => s.ClassId)
            //    .WillCascadeOnDelete(true);

            //modelBuilder.Entity<Student>()
            //    .HasMany(s => s.StudentClasses)
            //    .WithRequired(s => s.Student)
            //    .HasForeignKey(s => s.StudentId)
            //    .WillCascadeOnDelete(true);
        }

        public System.Data.Entity.DbSet<ScholarMate.Core.Models.Schools.Staff> Staffs { get; set; }
    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}