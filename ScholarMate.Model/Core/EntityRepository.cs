﻿using ScholarMate.Core.Models.Base;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ScholarMate.Core
{
    public class EntityRepository<T, K> : IEntityRepository<T, K>
        where T : EntityWithTypedId<K>,  new() where K : IComparable, IFormattable,
    IConvertible, IComparable<K>, IEquatable<K>
    {

        readonly DbContext _dbContext;

        public EntityRepository(DbContext dbContext) {

            if (dbContext == null) {

                throw new ArgumentNullException("dbContext");
            }

            _dbContext = dbContext;
        }

        public virtual IQueryable<T> GetAll() {

            return _dbContext.Set<T>();
        }

        public virtual IQueryable<T> AllIncluding(
            params Expression<Func<T, object>>[] includeProperties) {

            IQueryable<T> query = _dbContext.Set<T>();
            foreach (var includeProperty in includeProperties) {

                query = query.Include(includeProperty);
            }

            return query;
        }

        public T GetSingle(K key) {

            return GetAll().FirstOrDefault(x => x.Id.Equals(key));
        }

        public virtual IQueryable<T> FindBy(Expression<Func<T, bool>> predicate) {

            return _dbContext.Set<T>().Where(predicate);
        }

        public virtual PaginatedList<T> Paginate<TKey>(
                    int pageIndex, int pageSize,
                    Expression<Func<T, TKey>> keySelector) {

            return Paginate(pageIndex, pageSize, keySelector, null);
        }

        public virtual PaginatedList<T> Paginate<TKey>(
            int pageIndex, int pageSize,
            Expression<Func<T, TKey>> keySelector,
            Expression<Func<T, bool>> predicate,
            params Expression<Func<T, object>>[] includeProperties) {

            IQueryable<T> query = 
                AllIncluding(includeProperties).OrderBy(keySelector);

            query = (predicate == null) 
                ? query 
                : query.Where(predicate);

            return query.ToPaginatedList(pageIndex, pageSize);
        }

        public virtual void Add(T entity) {

            DbEntityEntry dbEntityEntry = _dbContext.Entry<T>(entity);
            _dbContext.Set<T>().Add(entity);
        }

        public virtual void Edit(T entity) {

            DbEntityEntry dbEntityEntry = _dbContext.Entry<T>(entity);
            dbEntityEntry.State = EntityState.Modified;
        }

        public virtual void Delete(T entity) {

            DbEntityEntry dbEntityEntry = _dbContext.Entry<T>(entity);
            dbEntityEntry.State = EntityState.Deleted;
        }

        public virtual void DeleteGraph(T entity) {

            DbSet<T> dbSet = _dbContext.Set<T>();
            dbSet.Attach(entity);
            dbSet.Remove(entity);
        }

        public virtual void Save() {

            _dbContext.SaveChanges();
        }
    }
}