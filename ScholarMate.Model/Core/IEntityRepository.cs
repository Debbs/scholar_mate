﻿using ScholarMate.Model.Models.Base;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace ScholarMate.Model
{

    public interface IEntityRepository<T, K>  where T : IEntityWithTypedId<K> , new() where K : IComparable, IFormattable,
    IConvertible, IComparable<K>, IEquatable<K>
    {

        IQueryable<T> AllIncluding(params Expression<Func<T, object>>[] includeProperties);
        IQueryable<T> GetAll();
        T GetSingle(K id);
        IQueryable<T> FindBy(Expression<Func<T, bool>> predicate);

        PaginatedList<T> Paginate<TKey>(
            int pageIndex, int pageSize,
            Expression<Func<T, TKey>> keySelector);

        PaginatedList<T> Paginate<TKey>(
            int pageIndex, int pageSize,
            Expression<Func<T, TKey>> keySelector,
            Expression<Func<T, bool>> predicate,
            params Expression<Func<T, object>>[] includeProperties);

        void Add(T entity);
        void Edit(T entity);
        void Delete(T entity);
        void DeleteGraph(T entity);
        void Save();
    }
}